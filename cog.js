/* global log */
/* global adapters */
/* global config */
/* global cogs */
/* global pronghornProps */

const itentialPromisify = require('@itentialopensource/itentialpromisify');
const wfbPromise = itentialPromisify(cogs.WorkflowBuilder);

function searchWorkflows(searchQuery) {
  return new Promise(function (resolve, reject) {
    try {
      cogs.WorkFlowEngine.searchWorkflows(searchQuery, (data, error) => {
        if (error) {
          log.error(error);
          reject(error);
        }
        if (data) {
          resolve(data.results);
        } else {
          log.error(error);
          reject(error);
        }
      });
    } catch (error) {
      log.error(error);
      reject(error);
    }
  });
}


async function exportWorkflow(workflow) {
  const exportOptions = {
    'name': workflow
  };
  try {
    const data = await wfbPromise.exportWorkflow(exportOptions);
    if (data) {
      return data;
    }
    log.error('Unable to export workflow');
    return 'Unable to export workflow';
  } catch (error) {
    log.error(error);
    return error;
  }
}


async function importWorkflow(workflow) {
  try {
    const data = await wfbPromise.importWorkflow(workflow, {});
    if (data) {
      return data;
    }
    log.error('Unable to import workflow');
    return 'Unable to import workflow';
  } catch (error) {
    log.error(error);
    return error;
  }
}

async function deleteWorkflow(workflow) {
  try {
    const data = await wfbPromise.deleteWorkflow(workflow);
    if (data) {
      return data;
    }
    log.error('Unable to delete workflow');
    return 'Unable to delete workflow';
  } catch (error) {
    log.error(error);
    return error;
  }
}

/**
   * This is the pronghorn.json global section. It has to be marked with @name
   * pronghornTitleBlock tag
   * @name pronghornTitleBlock
   * @pronghornId @clinkConsumer/app-consumer
   * @title AppConsumer
   * @export AppConsumer
   * @type Application
   * @summary Custom application for Centurylink consumer team.
   * @src cog.js
   * @encrypted false
   * @roles admin engineering support apiread apiwrite
   */
class TaskConverter {
  constructor() {
    log.info("Starting TaskConverter");
  }

  /**
     * @pronghornType method
     * @name replaceWithViewData
     * @summary Copies workflow tasks between 2 workflows
     * @param {string} workflow Name of the workflow
     * @param {boolean} overwrite Overwrite the workflow?
     * @return {object} Replaced Tasks
     * @route {POST} /replaceWithViewData
     * @roles admin
     * @task true
     */
  async replaceWithViewData(workflow, overwrite, callback) {
    let exportedWorkflow; let importResponse;
    let replacedTasks = {};
    try {
      exportedWorkflow = await exportWorkflow(workflow);
    } catch (error) {
      log.error(error);
      return callback(null, `Unable to find workflow : ${workflow}`);
    }
    for (const i of Object.keys(exportedWorkflow['tasks'])) {
      if (exportedWorkflow['tasks'][i]["name"] == "confirmTask" || exportedWorkflow['tasks'][i]["name"] == "decisionTask") {
        replacedTasks[i] = `${exportedWorkflow['tasks'][i]["name"]} changed to View Data`
        exportedWorkflow['tasks'][i]["name"] = "ViewData";
        if (exportedWorkflow['tasks'][i]["summary"] == "MOP Decision Task" || exportedWorkflow['tasks'][i]["summary"] == "MOP confirm Task") {
          exportedWorkflow['tasks'][i]["summary"] = "View Data";
        }
        if (exportedWorkflow['tasks'][i]["description"] == "MOP Decision Task" || exportedWorkflow['tasks'][i]["description"] == "MOP confirm Task") {
          exportedWorkflow['tasks'][i]["description"] = "View data in a pretty formatView Data";
        }
        exportedWorkflow['tasks'][i]["view"] = "/tools/task/ViewData";
        exportedWorkflow['tasks'][i]["displayName"] = "Tools";
        exportedWorkflow['tasks'][i]["app"] = "Tools";
        exportedWorkflow['tasks'][i]["deprecated"] = false;
        exportedWorkflow['tasks'][i]["matched"] = [
            {
                "key": "name",
                "highlightString": "<span class='highlight-string'>View</span>Data"
            },
            {
                "key": "summary",
                "highlightString": "<span class='highlight-string'>View</span> Data"
            },
            {
                "key": "description",
                "highlightString": "<span class='highlight-string'>View</span> data in a pretty format"
            }
        ];
        let incomingVariables = exportedWorkflow['tasks'][i]["variables"]["incoming"];
        delete exportedWorkflow['tasks'][i]["variables"]["incoming"];
        exportedWorkflow['tasks'][i]["variables"]["incoming"] = {};
        exportedWorkflow['tasks'][i]["variables"]["incoming"]["header"] = incomingVariables["title"];
        exportedWorkflow['tasks'][i]["variables"]["incoming"]["message"] = "";
        exportedWorkflow['tasks'][i]["variables"]["incoming"]["body"] = incomingVariables["body"];
        if (!("btn_success" in incomingVariables)) {
          exportedWorkflow['tasks'][i]["variables"]["incoming"]["btn_success"] = "Success";
          exportedWorkflow['tasks'][i]["variables"]["incoming"]["btn_failure"] = "";
        }
        else {
          exportedWorkflow['tasks'][i]["variables"]["incoming"]["btn_success"] = incomingVariables["btn_success"];
          exportedWorkflow['tasks'][i]["variables"]["incoming"]["btn_failure"] = incomingVariables["btn_failure"];
        }
      }
      }
    if (overwrite) {
      try {
        await deleteWorkflow(workflow);
      } catch (error) {
        log.error(error);
        return callback(null, `Unable to delete workflow : ${workflow}`);
      }
    }
    try {
      importResponse = await importWorkflow(exportedWorkflow);
      } catch (error) {
      log.error(error);
      return callback(null, `Unable to import workflow : ${workflow}`);
    }
    return callback({[importResponse["name"]] : replacedTasks});
  
}
}


module.exports = new TaskConverter();
