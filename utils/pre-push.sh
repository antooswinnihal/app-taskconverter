#!/bin/sh

# exit on any failure in the pipeline
set -e

# -------------------------------------------------
# pre-commit
# -------------------------------------------------
# Contains the standard set of tasks to run before
# committing changes to the repo. If any tasks fail
# then the commit will be aborted.
# -------------------------------------------------

printf "%b" "Running pre-commit hooks...\\n"

# validate the package.json file
node utils/validate.js

# lint the code
npm run lint

# run the unit tests
npm run test:unit

printf "%b" "Finished running pre-commit hooks\\n"
